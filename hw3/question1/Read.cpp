//Author: Sijia Zhang
//create time: 18/11/2017

#include <iostream>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>

using namespace boost::interprocess;
/*Our aim is publish int between two C++ programs using shared memory.*/
//This is the program to read "int"

int main(){
    //open "Try"
    shared_memory_object shdmem1(open_only, "Try", read_write);

    //create region2 which is used to read the shared memory in same location
    mapped_region region2(shdmem1, read_only);

    //write the number out
    int *i= static_cast<int*>(region2.get_address());

    //print
    std::cout<<"The integer in shared_memory is:"<<*i<<std::endl;

    //delete shared memory
    bool remove=shared_memory_object::remove("Try");
    //check whether it has been deleted
    std::cout<<std::boolalpha<<"Is Try has been deleted?"<<remove<<std::endl;

    return 0;
}
