//Author: Sijia Zhang
//create time: 18/11/2017

#include <iostream>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>

using namespace boost::interprocess;
/*Our aim is publish int between two C++ programs using shared memory.*/
//This is the program to publish//

int main(){
    //firstly, we need to create and size the memory, the name "shdmem" is called according to the reference https://theboostcpplibraries.com/boost.interprocess-shared-memory
    shared_memory_object shdmem(open_or_create, "Try", read_write);
    shdmem.truncate(sizeof(int)); //because our case ask as to publish int bwtween 2 programs

    //Then, we write a number in shdmem
    mapped_region region1(shdmem, read_write);
    int *i1= static_cast<int*>(region1.get_address());
    *i1=55;

    return 0;
}
