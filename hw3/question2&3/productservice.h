//Author: Sijia Zhang
//create time: 15/11/2017

/**
 * productservice.hpp defines Bond and IRSwap ProductServices
 */

#include <iostream>
#include <map>
#include <vector>
#include "Product.h"
#include "soa.h"

/**
 * Bond Product Service to own reference data over a set of bond securities.
 * Key is the productId string, value is a Bond.
 */
class BondProductService : public Service<string,Bond>
{

public:
    // BondProductService ctor
    BondProductService();

    // Return the bond data for a particular bond product identifier
    Bond& GetData(string productId);

    // Add a bond to the service (convenience method)
    void Add(Bond &bond);

    // Get all Bonds with the specified ticker
    std::vector<Bond> GetBonds(string& _ticker);
private:
    map<string,Bond> bondMap; // cache of bond products

};

//Write the following utility methods on the IRSwapProductService to search for all instances of an IRSwap for a particular attribute.
/**
 * Interest Rate Swap Product Service to own reference data over a set of IR Swap products
 * Key is the productId string, value is a IRSwap.
 */
class IRSwapProductService : public Service<string,IRSwap>
{
public:
    // IRSwapProductService ctor
    IRSwapProductService();

    // Return the IR Swap data for a particular bond product identifier
    IRSwap& GetData(string productId);

    // Add a bond to the service (convenience method)
    void Add(IRSwap &swap);

    // Get all Swaps with the specified fixed leg day count convention
    vector<IRSwap> GetSwaps(DayCountConvention _fixedLegDayCountConvention);

    // Get all Swaps with the specified fixed leg payment frequency
    vector<IRSwap> GetSwaps(PaymentFrequency _fixedLegPaymentFrequency);

    // Get all Swaps with the specified floating index
    vector<IRSwap> GetSwaps(FloatingIndex _floatingIndex);

    // Get all Swaps with a term in years greater than the specified value
    vector<IRSwap> GetSwapsGreaterThan(int _termYears);

    // Get all Swaps with a term in years less than the specified value
    vector<IRSwap> GetSwapsLessThan(int _termYears);

    // Get all Swaps with the specified swap type
    vector<IRSwap> GetSwaps(SwapType _swapType);

    // Get all Swaps with the specified swap leg type
    vector<IRSwap> GetSwaps(SwapLegType _swapLegType);

    //define a new member function here
    //This function is used to return all swaps satisfy the requirement.
    std::vector<IRSwap> requirment_follower(std::function<bool(IRSwap &)> fun){
        std::vector<IRSwap> container;//a vector to contain all swaps follow requirement.
        for(auto i : swapMap){
            if(fun(i.second)) //if swap follow the requirment, push back!
                container.push_back(i.second);
        }
        return container;
    }

private:
    map<string,IRSwap> swapMap; // cache of IR Swap products
};

BondProductService::BondProductService()
{
    bondMap = map<string,Bond>();
}

Bond& BondProductService::GetData(string productId)
{
    return bondMap[productId];
}

void BondProductService::Add(Bond &bond)
{
    bondMap.insert(pair<string,Bond>(bond.GetProductId(), bond));
}


//Get all Bonds with the specified ticker
std::vector<Bond> BondProductService::GetBonds(std::string & _ticker){
    std::vector<Bond> container; //create a vector to store the result
    for(auto i : bondMap){
        if(i.second.GetTicker() == _ticker) //to check whether the ticker is equal
            container.push_back(i.second); //store bonds which has same ticker with input
    }
    return container;
}


IRSwapProductService::IRSwapProductService()
{
    swapMap = map<string,IRSwap>();
}

IRSwap& IRSwapProductService::GetData(string productId)
{
    return swapMap[productId];
}

void IRSwapProductService::Add(IRSwap &swap)
{
    swapMap.insert(pair<string,IRSwap>(swap.GetProductId(), swap));
}

// Get all Swaps with the specified fixed leg day count convention
std::vector<IRSwap> IRSwapProductService::GetSwaps(DayCountConvention _fixedLegDayCountConvention){
    //using lambda function to get requirment_follower function
    return requirment_follower([=](IRSwap &swap){ return swap.GetFixedLegDayCountConvention() == _fixedLegDayCountConvention;});
}

// Get all Swaps with the specified fixed leg payment frequency
std::vector<IRSwap> IRSwapProductService::GetSwaps(PaymentFrequency _fixedLegPaymentFrequency){
    //using lambda function to get requirment_follower function
    return requirment_follower([=](IRSwap &swap){ return swap.GetFixedLegPaymentFrequency() == _fixedLegPaymentFrequency;});
}

// Get all Swaps with the specified floating index
std::vector<IRSwap> IRSwapProductService::GetSwaps(FloatingIndex _floatingIndex){
    //using lambda function to get requirment_follower function
    return requirment_follower([=](IRSwap &swap){ return swap.GetFloatingIndex() == _floatingIndex;});
}

// Get all Swaps with a term in years greater than the specified value
std::vector<IRSwap> IRSwapProductService::GetSwapsGreaterThan(int _termYears){
    //using lambda function to get requirment_follower function
    return requirment_follower([=](IRSwap &swap){ return swap.GetTermYears() > _termYears;});
}

// Get all Swaps with a term in years less than the specified value
std::vector<IRSwap> IRSwapProductService::GetSwapsLessThan(int _termYears){
    //using lambda function to get requirment_follower function
    return requirment_follower([=](IRSwap &swap){ return swap.GetTermYears() < _termYears;});
}

// Get all Swaps with the specified swap type
std::vector<IRSwap> IRSwapProductService::GetSwaps(SwapType _swapType){
    //using lambda function to get requirment_follower function
    return requirment_follower([=](IRSwap &swap){ return swap.GetSwapType() == _swapType;});
}

// Get all Swaps with the specified swap leg type
std::vector<IRSwap> IRSwapProductService::GetSwaps(SwapLegType _swapLegType){
    //using lambda function to get requirment_follower function
    return requirment_follower([=](IRSwap &swap){ return swap.GetSwapLegType() == _swapLegType;});
}
