//Author: Sijia Zhang
//create time: 15/11/2017

#include <iostream>
#include "soa.h"
#include "Future.h"
#include "BondFuture.h"
#include "EuroDollarFuture.h"
#include "FutureProductService.h"
#include "productservice.h"
using namespace std;

//This main function is used to test future, bond and swaps.
int main(){
  std::cout<<"***1. test Futures***"<<std::endl;

  //create several futures.
  //a eurodollar future.
  std::string id1="future_1"; //create the id of the future.
  date maturity1(2019, Jan, 05); //create the maturity date.
  //create a EuroDollarFuture with country USA and number of coupon payments is 4.
  EuroDollarFuture future1(id1,"Ticker0",maturity1, FutureType::EURODOLLARfuture, FUTUREidtype1, "USA", 4);


  //create a bond future
  std::string id2="future_2"; //create the id of the future.
  date maturity2(2018, Apr, 07); //create the maturity date.
  //create a bond future with coupon rate 3% and number of coupon payments is 2.
  BondFuture future2(id2,"Ticker0",maturity2, FutureType::BONDfuture, FUTUREidtype1, 0.03, 2);

  //create a commodity future
  std::string id3="future_3"; //create the id of the future.
  date maturity3(2020, Sep, 22); //create the maturity date.
  //create a bond future with coupon rate 3% and number of coupon payments is 2.
  Future future3(id3,"Ticker0",maturity2, FutureType::COMMODITYfuture, FUTUREidtype1);

  //check << operator in Bond future and eurodollar future.
  std::cout<<"Give the information of eurodollar future!"<<std::endl;
  std::cout<<future1<<std::endl; //print info of eurodollar future
  std::cout<<std::endl;

  std::cout<<"Give the information of bond future!"<<std::endl;
  std::cout<<future2<<std::endl; //print info of bond future
  std::cout<<std::endl;

  //test future product service.
  std::cout<<"***1.1 future product service check***"<<std::endl;
  //create a future product service.
  //we use smart pointer for convenience.
  std::shared_ptr<FutureProductService> pt1(new FutureProductService);

  //Add member function
  pt1->Add(future1);
  pt1->Add(future2);
  pt1->Add(future3);

  //GetData member function
  auto new_future=pt1->GetData(id1); //get the future with id1.
  //print
  std::cout<<new_future.GetProductId()<<" ==> "<<new_future<<std::endl;
  std::cout<<std::endl;





  //Then test Bond.
  std::cout<<"***2. Test Bond***"<<std::endl;
  //create several bonds, using examples from the lecture.
  // Create the 10Y treasury note
  date maturityDate(2025, Nov, 16);
  string cusip = "912828M56";
  Bond treasuryBond(cusip, CUSIP, "T", 2.25, maturityDate);

  // Create the 2Y treasury note
  date maturityDate2(2017, Nov, 5);
  string cusip2 = "912828TW0";
  Bond treasuryBond2(cusip2, CUSIP, "T", 0.75, maturityDate2);

  // Create a BondProductService
  BondProductService *bondProductService = new BondProductService();

  // Add the 10Y note to the BondProductService and retrieve it from the service
  bondProductService->Add(treasuryBond);
  Bond bond = bondProductService->GetData(cusip);
  cout << "CUSIP: " << bond.GetProductId() << " ==> " << bond << endl;

  // Add the 2Y note to the BondProductService and retrieve it from the service
  bondProductService->Add(treasuryBond2);
  bond = bondProductService->GetData(cusip2);
  cout << "CUSIP: " << bond.GetProductId() << " ==> " << bond << endl;
  std::cout<<std::endl;

  //test GetBond.
  //find all bonds with ticker "T"
  std::cout<<"find all bonds with ticker T"<<std::endl;
  std::string find="T";
  auto new_bond=bondProductService->GetBonds(find);
  //print
  for(auto i:new_bond){
    std::cout<<i<<std::endl;
  }
  std::cout<<std::endl;



  //finally, test swaps.
  std::cout<<"***3. Test Swaps***"<<std::endl;
  //create swaps using example in the lecture.
  // Create the Spot 10Y Outright Swap
  date effectiveDate(2015, Nov, 16);
  date terminationDate(2025, Nov, 16);
  string outright10Y = "Spot-Outright-10Y";
  IRSwap outright10YSwap(outright10Y, THIRTY_THREE_SIXTY, THIRTY_THREE_SIXTY, SEMI_ANNUAL, LIBOR, TENOR_3M, effectiveDate, terminationDate, USD, 10, SPOT, OUTRIGHT);

  // Create the IMM 2Y Outright Swap
  date effectiveDate2(2015, Dec, 20);
  date terminationDate2(2017, Dec, 20);
  string imm2Y = "IMM-Outright-2Y";
  IRSwap imm2YSwap(imm2Y, THIRTY_THREE_SIXTY, THIRTY_THREE_SIXTY, SEMI_ANNUAL, LIBOR, TENOR_3M, effectiveDate2, terminationDate2, USD, 2, IMM, OUTRIGHT);

  // Create a IRSwapProductService
  IRSwapProductService *swapProductService = new IRSwapProductService();

  // Add the Spot 10Y Outright Swap to the IRSwapProductService and retrieve it from the service
  swapProductService->Add(outright10YSwap);
  IRSwap swap = swapProductService->GetData(outright10Y);
  cout << "Swap: " << swap.GetProductId() << " == > " << swap << endl;

  // Add the IMM 2Y Outright Swap to the IRSwapProductService and retrieve it from the service
  swapProductService->Add(imm2YSwap);
  swap = swapProductService->GetData(imm2Y);
  cout << "Swap: " << swap.GetProductId() << " == > " << swap << endl;
  std::cout<<std::endl;

  //new added memeber function
  //GetSwaps
  std::cout << "fixedLegDayCountConvention!" <<std::endl;
  auto it=swapProductService->GetSwaps(THIRTY_THREE_SIXTY);
  //print
  for(auto i:it){
    std::cout<<i<<std::endl;
  }
  std::cout<<std::endl;

  std::cout << "fixedLegPaymentFrequency!" <<std::endl;
  auto it1=swapProductService->GetSwaps(ANNUAL);
  //print
  for(auto i:it1){
    std::cout<<i<<std::endl;
  }
  std::cout<<std::endl;

  std::cout << "floatingindex!" <<std::endl;
  auto it2=swapProductService->GetSwaps(LIBOR);
  //print
  for(auto i:it2){
    std::cout<<i<<std::endl;
  }
  std::cout<<std::endl;

  std::cout << "Greater than a specific term!" <<std::endl;
  auto it3=swapProductService->GetSwapsGreaterThan(3);
  //print
  for(auto i:it3){
    std::cout<<i<<std::endl;
  }
  std::cout<<std::endl;

  std::cout << "Smaller than a specific term!" <<std::endl;
  auto it4=swapProductService->GetSwapsGreaterThan(5);
  //print
  for(auto i:it4){
    std::cout<<i<<std::endl;
  }
  std::cout<<std::endl;

  std::cout << "Swap type!" <<std::endl;
  auto it5=swapProductService->GetSwaps(OUTRIGHT);
  //print
  for(auto i:it5){
    std::cout<<i<<std::endl;
  }
  std::cout<<std::endl;

  return 0;
}
