//Author: Sijia Zhang
//Create time: 11/15/2017

#ifndef EuroDollarFuture_HPP
#define EuroDollarFuture_HPP

#include <iostream>
#include <string>
#include "Future.h"

using namespace boost::gregorian;

//It is a subclass of class future.
class EuroDollarFuture: public Future{
private:
    std::string country_type; //define the country of future
    int payment_times; //define number of coupon payments each year
public:
    //create the constructor
    EuroDollarFuture(std::string productid0, std::string ticker0, date maturity0, FutureType futuretype0, FutureIDType futureidtype0,
                     std::string country_type0, int payment0):Future(productid0, ticker0, maturity0, futuretype0, futureidtype0){
        country_type=country_type0;
        payment_times=payment0;
    }

    EuroDollarFuture() = default;

    //Return the country type
    std::string GetCountry() const;

    //Return the payment time
    int GetPaymentTimes() const;

    //Overload the << operation to print out the future
    friend ostream& operator<<(ostream& output, const EuroDollarFuture&future);
};

//Return the country type
std::string EuroDollarFuture::GetCountry() const{
    return country_type;
}

//Return the payment time
int EuroDollarFuture::GetPaymentTimes() const{
    return payment_times;
}

//Overload the << operation to print out the future
ostream& operator<<(ostream& output, const EuroDollarFuture& future){
    output << static_cast<Future>(future);
    output << "Country is:"<<future.GetCountry()<<" "<<", and payment times per year is:"<<future.GetPaymentTimes();
    return output;
}

#endif //EuroDollarFuture_HPP
