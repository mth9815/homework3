//Author: Sijia Zhang
//Create time: 11/15/2017

#ifndef BondFuture_HPP
#define BondFuture_HPP

#include <iostream>
#include <string>
#include "Future.h"

using namespace boost::gregorian;

//It is a subclass of class future.
class BondFuture: public Future{
private:
    double coupon; //define coupon rate each time
    int payment_times; //define number of payments of coupon each year
public:
    //create the constructor
    BondFuture(std::string productid0, std::string ticker0, date maturity0, FutureType futuretype0, FutureIDType futureidtype0,
                     double coupon0, int payment0):Future(productid0, ticker0, maturity0, futuretype0, futureidtype0){
        coupon=coupon0;
        payment_times=payment0;
    }

    BondFuture() = default;

    //Return the coupon rate
    double GetCoupon() const;

    //Return the payment time
    int GetPaymentTimes() const;

    //Overload the << operation to print out the future
    friend ostream& operator<<(ostream& output, const BondFuture& future);
};

//Return the coupon rate
double BondFuture::GetCoupon() const{
    return coupon;
}

//Return the payment time
int BondFuture::GetPaymentTimes() const{
    return payment_times;
}

//Overload the << operation to print out the future
ostream& operator<<(ostream& output, const BondFuture& future){
    output << static_cast<Future>(future);
    output << "Coupon rate is:"<<future.GetCoupon()<<" "<<", and payment times per year is:"<<future.GetPaymentTimes();
    return output;
}
#endif //BondFuture_HPP
