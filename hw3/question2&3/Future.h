//Author: Sijia Zhang
//Create time: 11/15/2017

#ifndef Future_HPP
#define Future_HPP

#include "Product.h"

using namespace boost::gregorian;

//This class is the same way as Bond and IRSwap, it inherits from class Product.

//using enum to define types of futures.
enum class FutureType {EURODOLLARfuture, BONDfuture, COMMODITYfuture};
//using enum to define future id type
enum FutureIDType{FUTUREidtype1};

class Future : public Product{
private:
    std::string product_id; //a string to show product ID.
    std::string ticker; //a string to show ticker
    date maturity; //date to show maturity date
    FutureType future_type; //define future type
    FutureIDType future_id_type; //define future id type
public:
    //constructor
    Future(std::string productid0, std::string ticker0, date maturity0, FutureType futuretype0, FutureIDType futureidtype0): Product(productid0, FUTURE){
        future_id_type=futureidtype0;
        ticker=ticker0;
        maturity=maturity0;
        future_type=futuretype0;
    }

    Future(): Product(0, FUTURE){}

    //Return the ticker of the future
    std::string GetTicker() const;

    //Return the maturity date of the future
    date GetMaturityDate() const;

    //Return the future identifier type
    FutureIDType GetFutureIdType() const;

    //Return the future type
    FutureType GetFutureType() const;

    //Overload the << operator to print out the future
    friend ostream& operator<<(ostream& output, const Future& future);
};

//Return the ticker of the future.
std::string Future::GetTicker() const {
    return ticker;
}

//Return the maturity date of the future
date Future::GetMaturityDate() const{
    return maturity;
}

//Return the future identifier type
FutureIDType Future::GetFutureIdType() const{
    return future_id_type;
}

//Return the future type
FutureType Future::GetFutureType() const {
    return future_type;
}

//Overload the << operator to print out the future
ostream& operator<<(ostream& output, const Future& future){
    //add a function to get the type of the future.
    switch(future.future_type){
        case FutureType::BONDfuture:
            output<<" "<<"Bond Future";
            break;
        case FutureType::COMMODITYfuture:
            output<<" "<<"Commodity Future";
            break;
        case FutureType::EURODOLLARfuture:
            output<<" "<<"Eurodollar future";
            break;
        //others print error
        default:
            output<<"error!";
    }
    output<<", "<<future.ticker<<", "<<future.GetMaturityDate();
    return output;
}

#endif //Future_HPP
