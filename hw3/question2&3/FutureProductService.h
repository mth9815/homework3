//Author: Sijia Zhang
//Create time: 11/15/2017

#ifndef FutureProductService_HPP
#define FutureProductService_HPP

#include <iostream>
#include <map>
#include "Product.h"
#include "Future.h"
#include "soa.h"
//This class is likely to a BondProductService which is to own reference data over a set of Future Securities.
/*Key is the productId string, value is a future.*/

class FutureProductService: public Service<std::string, Future>{
private:
    std::map<std::string, Future> future_map; //cache of future products
public:
    //FutureProductService constructor
    FutureProductService();

    //Return the Future data for a particular Future product identifier
    Future& GetData(string productId);

    //Add a future to the service (convenience method)
    void Add(Future &future);
};

//FutureProductService constructor
FutureProductService::FutureProductService()
{
    future_map = map<string,Future>();
}

//Return the Future data for a particular Future product identifier
Future& FutureProductService::GetData(string productId)
{
    return future_map[productId];
}

//Add a future to the service (convenience method)
void FutureProductService::Add(Future &future)
{
    future_map.insert(pair<string,Future>(future.GetProductId(), future));
}

#endif //FutureProductService_HPP
