# README #

### What is this project for? ###

The repo is for MTH 9815 SOA - ProductService Homework

There are three parts:

1. Homework 1

2. Homework 2&3

### How to run the code? ###

Homework1:

please change your directory to /question1 and then using ./publish and ./read to see the result.

If you would like to recompile the file, please use:

g++ -I diretory_to_boost Read.cpp -o read -lrt
g++ -I diretory_to_boost Publish.cpp -o publish -lrt

Homework2&3:

please change your directory to /question2&3/cmake-build-debug and then using ./HW2 to run

### Members of the group ###
Sijia Zhang, Zhirong Zhang, Ziyi Wang, Tiangang Zhang

Repo owner: ziyiwang95@gmail.com